package com.example.recyclerview_alphabet_fast_scroller

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.viethoa.RecyclerViewFastScroller.BubbleTextGetter


class RecyclerViewAdapter(val context : Context, val mDataArray: List<String>?) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(), BubbleTextGetter {

    override fun getItemCount(): Int {
        return mDataArray?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(context).inflate(R.layout.item_recycler_view_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.text!!.text = mDataArray!![position]
    }

    override fun getTextToShowInBubble(pos: Int): String? {

        if (pos < 0 || pos >= mDataArray!!.size){
            return null
        }
        else{

            val name = mDataArray[pos]
            if (name == null || name.length < 1)
                return null
            else
                return  mDataArray[pos].substring(0, 1)

        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        val text = itemView?.findViewById<TextView>(R.id.tv_alphabet)

    }

}