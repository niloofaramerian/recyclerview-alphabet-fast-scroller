package com.example.recyclerview_alphabet_fast_scroller

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.recyclerview_alphabet_fast_scroller.DataHelper.getAlphabetData
import com.viethoa.RecyclerViewFastScroller
import com.viethoa.models.AlphabetItem
import java.util.*


class MainActivity : AppCompatActivity() {

    var mRecyclerView: RecyclerView? = null
    var fastScroller: RecyclerViewFastScroller? = null
    private var mDataArray: List<String>? = null
    private var mAlphabetItems: MutableList<AlphabetItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        mRecyclerView = findViewById(R.id.my_recycler_view)
        fastScroller = findViewById(R.id.fast_scroller)

        initialiseData()
        initialiseUI()
    }

    protected fun initialiseData() {
        //Recycler view data
        mDataArray = DataHelper.getAlphabetData()

        //Alphabet fast scroller data
        mAlphabetItems = ArrayList()
        val strAlphabets: MutableList<String> = ArrayList()

        for (i in mDataArray!!.indices) {
            val name = mDataArray!![i]
            if (name == null || name.trim { it <= ' ' }.isEmpty()) continue
            val word = name.substring(0, 1)
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word)
                (mAlphabetItems as ArrayList<AlphabetItem>).add(AlphabetItem(i, word, false))
            }
        }
    }

    protected fun initialiseUI() {

        mRecyclerView?.layoutManager = LinearLayoutManager(this)
        mRecyclerView?.adapter = RecyclerViewAdapter(this , mDataArray)

        fastScroller?.setRecyclerView(mRecyclerView)
        fastScroller?.setUpAlphabet(mAlphabetItems)
    }
}