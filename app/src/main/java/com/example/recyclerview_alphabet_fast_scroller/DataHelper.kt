package com.example.recyclerview_alphabet_fast_scroller

import java.util.*

object DataHelper {
    fun getAlphabetData() : List<String>{

        val alphabetData = mutableListOf<String>()

        alphabetData.add("B item 1")
        alphabetData.add("B item 2")
        alphabetData.add("B item 3")
        alphabetData.add("B item 4")
        alphabetData.add("B item 5")

        alphabetData.add("C item 1")
        alphabetData.add("C item 2")
        alphabetData.add("C item 3")
        alphabetData.add("C item 4")
        alphabetData.add("C item 5")

        alphabetData.add("D item 1")
        alphabetData.add("D item 2")
        alphabetData.add("D item 3")
        alphabetData.add("D item 4")
        alphabetData.add("D item 5")

        alphabetData.add("E item 1")
        alphabetData.add("E item 2")
        alphabetData.add("E item 3")
        alphabetData.add("E item 4")
        alphabetData.add("E item 5")

        alphabetData.add("F item 1")
        alphabetData.add("F item 2")
        alphabetData.add("F item 3")
        alphabetData.add("F item 4")
        alphabetData.add("F item 5")

        alphabetData.add("G item 1")
        alphabetData.add("G item 2")
        alphabetData.add("G item 3")
        alphabetData.add("G item 4")
        alphabetData.add("G item 5")

        alphabetData.add("H item 1")
        alphabetData.add("H item 2")
        alphabetData.add("H item 3")
        alphabetData.add("H item 4")
        alphabetData.add("H item 5")

        alphabetData.add("I item 1")
        alphabetData.add("I item 2")
        alphabetData.add("I item 3")
        alphabetData.add("I item 4")
        alphabetData.add("I item 5")

        alphabetData.add("J item 1")
        alphabetData.add("J item 2")
        alphabetData.add("J item 3")
        alphabetData.add("J item 4")
        alphabetData.add("J item 5")

        alphabetData.add("K item 1")
        alphabetData.add("K item 2")
        alphabetData.add("K item 3")
        alphabetData.add("K item 4")
        alphabetData.add("K item 5")

        alphabetData.add("L item 1")
        alphabetData.add("L item 2")
        alphabetData.add("L item 3")
        alphabetData.add("L item 4")
        alphabetData.add("L item 5")

        alphabetData.add("M item 1")
        alphabetData.add("M item 2")
        alphabetData.add("M item 3")
        alphabetData.add("M item 4")
        alphabetData.add("M item 5")

        alphabetData.add("N item 1")
        alphabetData.add("N item 2")
        alphabetData.add("N item 3")
        alphabetData.add("N item 4")
        alphabetData.add("N item 5")

        alphabetData.add("O item 1")
        alphabetData.add("O item 2")
        alphabetData.add("O item 3")
        alphabetData.add("O item 4")
        alphabetData.add("O item 5")

        alphabetData.add("P item 1")
        alphabetData.add("P item 2")
        alphabetData.add("P item 3")
        alphabetData.add("P item 4")
        alphabetData.add("P item 5")

        alphabetData.add("Q item 1")
        alphabetData.add("Q item 2")
        alphabetData.add("Q item 3")
        alphabetData.add("Q item 4")
        alphabetData.add("Q item 5")

        alphabetData.add("R item 1")
        alphabetData.add("R item 2")
        alphabetData.add("R item 3")
        alphabetData.add("R item 4")
        alphabetData.add("R item 5")

        alphabetData.add("S item 1")
        alphabetData.add("S item 2")
        alphabetData.add("S item 3")
        alphabetData.add("S item 4")
        alphabetData.add("S item 5")

        alphabetData.add("T item 1")
        alphabetData.add("T item 2")
        alphabetData.add("T item 3")
        alphabetData.add("T item 4")
        alphabetData.add("T item 5")

        alphabetData.add("U item 1")
        alphabetData.add("U item 2")
        alphabetData.add("U item 3")
        alphabetData.add("U item 4")
        alphabetData.add("U item 5")

        alphabetData.add("V item 1")
        alphabetData.add("V item 2")
        alphabetData.add("V item 3")
        alphabetData.add("V item 4")
        alphabetData.add("V item 5")

        alphabetData.add("W item 1")
        alphabetData.add("W item 2")
        alphabetData.add("W item 3")
        alphabetData.add("W item 4")
        alphabetData.add("W item 5")

        alphabetData.add("X item 1")
        alphabetData.add("X item 2")
        alphabetData.add("X item 3")
        alphabetData.add("X item 4")
        alphabetData.add("X item 5")

        alphabetData.add("Y item 1")
        alphabetData.add("Y item 2")
        alphabetData.add("Y item 3")
        alphabetData.add("Y item 4")
        alphabetData.add("Y item 5")

        alphabetData.add("Z item 1")
        alphabetData.add("Z item 2")
        alphabetData.add("Z item 3")
        alphabetData.add("Z item 4")
        alphabetData.add("Z item 5")


        return alphabetData
    }




}